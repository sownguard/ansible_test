from flask import Flask 
from config import Configuration
from flask import render_template
from create_db import Initialize
import time

app = Flask(__name__)
app.config.from_object(Configuration)
Initialize()

@app.route('/')
def index():
        wannatry = []
        with open('initdb.log', 'r') as output:
            while output.readline():
                wannatry.append(output.readline())
        
        return render_template('index.html', w = wannatry)
